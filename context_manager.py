class TestManager:
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        self.file = open(self.name, 'a+')
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(exc_val)
        if exc_type is ValueError:
            print('Value')
            print(exc_val)
        self.file.close()

манагер = TestManager('1.txt')
with манагер as e:
    e.write('asdasd')
    print(1)
    print(1)
    print(1)
    a = e.read()
    raise Exception('some exception')
    print(1)
    print(1)
print(a)

print('Second block')
if True is False:
    print(2)
    print(2.1)
    print(2.2)
print(3)
e.write('asd')
with TestManager('1.txt') as e:
    print(2)
    print(2)
    print(2)
    print(2)
    print(2)


class File:
    def __init__(self, name):
        pass
    def write(self, data):
        pass
    def read(self):
        return []

def open(name):
    return File(name)

class open:
    def __init__(self, name, attribute='r'):
        self.name = name
        self.attribute = attribute
    def __enter__(self):
        self.file = File('name')
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

with open('name', 'a+') as file:
    file.write('asdasd')
    a = file.read()
print(a)




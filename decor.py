from functools import wraps

def decor(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return result
    return wrapper

def func1(a, b, c, d):
    """Doc string"""
    return a, b, c, d

help(func1)
func2 = func1
help(func2)


func1 = decor(func1) #decoration
help(func1)


number_1 = 1
number_2 = number_1 # number_2 = 1
number_1 = 2
print(number_2)


lambda_1 = lambda a: a
lambda_2 = lambda_1
print(lambda_1(1))
print(lambda_2(1))

def decor(func):
    def wrapper(*args, **kwargs):
        print(1)
        result = func(*args, **kwargs)
        print(2)
        return result
    return wrapper
print(lambda_1(1000))
print()
lambda_1 = decor(lambda_1)
print(lambda_1(1000))
pass
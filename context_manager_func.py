from contextlib import contextmanager

# Контекстний менеджер здорової людини
def func1():
    with open('1.txt') as f:
        print(f.read())
func1()

# Контекстний менеджер курця
try:
    with open('1.txt') as f:
        def func2():
            print(f.read())

    func2()
except:
    pass

# Контекстний менеджер хітрого курця
@contextmanager
def cm(file_name):
    f = open(file_name)
    yield f
    f.close()

def func3():
    with cm('1.txt') as f:
        print(f.read())
func3()

@cm('1.txt')
def func4():
    print('')

func4()
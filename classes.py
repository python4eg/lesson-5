class Animal:
    CLASS_ATTR = 10

    def __init__(self, attr):
        self.attr = attr

    def sound(self):
        print(self.attr)


class Dog(Animal):
    def __init__(self, attr):
        super().__init__(attr)

    def wuf(self):
        self.sound()

dog1 = Dog('Гав!!!')
dog2 = Dog('Af!!!')


dog1.wuf()  # Hidden realization Dog.wuf(dog1)
dog2.wuf()  # Hidden realization Dog.wuf(dog2)

print(Dog.CLASS_ATTR)
print(dog1.CLASS_ATTR)
print(dog2.CLASS_ATTR)
Dog.CLASS_ATTR = 'New'
print(Dog.CLASS_ATTR)
print(dog1.CLASS_ATTR)
print(dog2.CLASS_ATTR)
dog1.CLASS_ATTR = 'Dog 1'
print(Dog.CLASS_ATTR)
print(dog1.CLASS_ATTR)
print(dog2.CLASS_ATTR)

print(Animal.__dict__)
print(Dog.__dict__)
print(dog1.__dict__)
print(dog2.__dict__)





a,b,c,d = [1,2,3,4]
print(a)
list_1 = [1,2,3,4]
print(list_1)

def func1():
    return 1, 2, 3, 4

g =func1()
print(g)

g = 1,2,3,4
g = 1,

a,b,c,d = set([1,2,3,4])
"""
"""
def func(a,b,c,d):
    pass

func(1,2,3,4)

args = 1,2,3,4
a,b,c,d = args
"""
"""
def func(*args):
    pass

func(1,2,3,4)

args = 1,2,3,4
args# => Goes to function as tuple
"""
"""
def func(a,b,c,d):
    pass

func(*[1,2,3,4])

args # Takes tuple from func call
a,b,c,d = args

"""
"""
h = dict(name='1')
print(h)

def func(a,b,c,d,name, key):
    pass

func(1,2,3,4,1, key=2)
args = [1,2,3,4,5]
kwargs = ['a','b','c','d','name','key']
kwargs = kwargs[len(args):]
print(kwargs)

def func1(a, b, *args, **kwargs):
    print(a)
    print(b)
    print(args)
    print(kwargs)

print(func1(1,2))
print(func1(1,2,6,7,8,54,4,5,6,2, den=10))

a, b, *args = [1,2,3,4,5,6,7,8,89,10]
print(a)
print(b)
print(args)

a,b = {'k1':1, 'k2':2}
print(a,b)